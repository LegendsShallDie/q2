#include "stack.h"
#include <iostream>

#define SIZE 10

void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* head = NULL;
	initStack(head);
	for (i = 0; i < size; i++)
	{
		push(&head, nums[i]);
	}
	i = 0;
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(&head);
	}
}

int* reverse10()
{
	int* nums = new int[SIZE];
	int i = 0;
	for (i = 0; i < SIZE; i++)
	{
		std::cin >> nums[i];
	}
	reverse(nums, SIZE);
	return nums;
}
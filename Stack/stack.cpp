#include "Utiles.h"
#include "stack.h"
#include <iostream>

#define SIZE 10
int main(void)
{
	stack* head = NULL;
	stack* curr = NULL;
	int i = 0;
	int nums[SIZE] = { 1,2,3,4,5,6,7,8,9,10 };
	initStack(head);
	push(&head, 1);
	push(&head, 2);
	push(&head, 3);
	push(&head, 4);
	push(&head, 5);
	pop(&head); // Return -1 if stack is 
	cleanStack(&head);
	printstack(head);
	reverse10();
	std::cout << "\n\n" << std::endl;
	for (i = 0; i < SIZE; i++)
	{
		std::cout << nums[i] << std::endl;

	}
	reverse(nums, 10);
	for (i = 0; i < SIZE; i++)
	{
		std::cout << nums[i] << std::endl;

	}
	i = 0;


	return 0;
}

/**
Function will print a stack of cells from head
input: linkedstack
output:
none
*/
void printstack(stack* head)
{

	stack* curr = head;
	int i = 0;
	if (!head) // empty stack!
	{
		std::cout <<  "Empty." << std::endl;
	}
	else
	{
		while (curr) // when curr == NULL, that is the end of the stack, and loop will end (NULL is false)
		{
			std::cout << curr->number << std::endl;

			curr = curr->next;
		}
		std::cout << "\n" << std::endl;
	}
}


/*
Function creates a cell to the stack.
*/
stack* createStackCell(unsigned int element)
{
	stack* newCell = new stack;
	newCell->number = element;
	newCell->next = NULL;
	return newCell;
}

/*
That function has literally no use in LinkedLists, since we don't need to assign size, leaving empty.
*/
void initStack(stack* s)
{
	s = createStackCell(0);
}

int pop(stack** s) // Return -1 if stack is empty
{
	stack* curr = *s;
	stack* last = curr;
	int out = -1;
	if (!*s) // empty stack!
	{
		std::cout << "Nothing to delete\n" << std::endl;
	}
	else if (!curr->next)
	{
		out = curr->number;
		delete curr;
		*s = 0;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			last = curr;
			curr = curr->next;
		}
		out = curr->number;
		delete curr;
		last->next = NULL;
	}
	return out;
}

/**
Function will add a cell to the stack
input:
head of stack
node to insert
output:
none
*/
void push(stack** s, unsigned int element)
{
	stack* newNode = createStackCell(element);
	stack* curr = *s;
	if (!*s) // empty stack!
	{
		*s = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}
		curr->next = newNode;
	}
	newNode->next = NULL;
}


/****************************************************\
Function will free all memory of a list of frames	 *
input:												 *
a list of frames									 *
output:												 *
none												 *
\****************************************************/
void cleanStack(stack** s)
{
	stack* temp = NULL;
	stack* curr = *s;
	while (curr)
	{
		temp = curr;
		curr = (curr)->next;
		delete temp;
	}

	*s = NULL;
}


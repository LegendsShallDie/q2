#include <stdio.h>
#include "LinkedList.h"


int main(void)
{
	list* head = NULL;
	list* curr = NULL;
	head = createCell(1);
	curr = createCell(2);
	insertAtEnd(&head, curr);
	curr = createCell(3);
	insertAtEnd(&head, curr);
	curr = createCell(4);
	insertAtEnd(&head, curr);
	TakeFromEnd(&head);
	TakeFromEnd(&head);
	TakeFromEnd(&head);
	TakeFromEnd(&head);


	printList(head);

	int num = 0, i = 0;

	return 0;
}

/**
Function will print a list of cells from head
input: linkedList
output:
none
*/
void printList(list* head)
{

	list* curr = head;
	int i = 0;
	if (!head) // empty list!
	{
		printf("Empty.");
	}
	else
	{
		while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
		{
			printf("%d ", curr->number);
			curr = curr->next;
		}
		printf("\n");
	}
}

/*
Function creates a new cell and assignes it with data
input:data
output: newCell
*/
list* createCell(unsigned int data)
{
	list* newCell = new list;
	newCell->number = data;
	newCell->next = NULL;
	return newCell;
}

void TakeFromEnd(list** head)
{
	list* curr = *head;
	list* last = NULL;
	if (!*head) // empty list!
	{
		printf("Nothing to delete\n");
	}
	else if(!curr->next)
	{
		delete curr;
		*head = 0;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			last = curr;
			curr = curr->next;
			
		}
		delete curr;
		last->next = NULL;
	}
	
}

/**
Function will add a cell to the list
input:
head of list
node to insert
output:
none
*/
void insertAtEnd(list** head, list* newNode)
{
	list* curr = *head;
	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}
		curr->next = newNode;
	}
	newNode->next = NULL;
}





#pragma once
#ifndef LinkedList_H
#define LinkedList_H

typedef struct list
{
	unsigned int number;
	struct list* next;
}list;

void TakeFromEnd(list** head);
list* createCell(unsigned int data);
void printList(list* head);
void insertAtEnd(list** head, list* newNode);

#endif /* LinkedList_H */